/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.grm.sdmmon.pack;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows giro component.
 */
public class giroAction extends AbstractAction {

    public giroAction() {
        super(NbBundle.getMessage(giroAction.class, "CTL_giroAction"));
//        putValue(SMALL_ICON, new ImageIcon(Utilities.loadImage(giroTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = giroTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}
