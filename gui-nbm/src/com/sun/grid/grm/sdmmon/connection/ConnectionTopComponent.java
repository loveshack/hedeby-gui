/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.connection;

import com.sun.grid.grm.sdmmon.scene.SdmSceneTopComponent;
import com.sun.grid.grm.sdmmon.options.OptionPanel;
import com.sun.grid.grm.sdmmon.*;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.sdmmon.edge.RootEdge;
import com.sun.grid.grm.sdmmon.logger.LoggerTopComponent;
import com.sun.grid.grm.sdmmon.node.ResourceProviderChildFactory;
import com.sun.grid.grm.sdmmon.node.ResourceProviderNode;
import com.sun.grid.grm.sdmmon.node.RootChildFactory;
import com.sun.grid.grm.sdmmon.node.RootNode;
import com.sun.grid.grm.sdmmon.node.ServiceChildFactory;
import com.sun.grid.grm.sdmmon.node.ServiceNode;
import com.sun.grid.grm.sdmmon.scene.SdmScene;
import com.sun.grid.grm.sdmmon.scene.SdmSceneImpl;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.sdmmon.widget.RootWidget;
import com.sun.grid.grm.sdmmon.widget.ServiceWidget;
import com.sun.grid.grm.service.Service;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.openide.util.Utilities;

/**
 * Top component which displays something.
 */
final class ConnectionTopComponent extends TopComponent implements LookupListener {

    private final static long serialVersionUID = -2008010401L;
    private static ConnectionTopComponent instance;
    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "com/sun/grid/grm/sdmmon/img/139.png";
    private static final String PREFERRED_ID = "ConnectionTopComponent";
    private Lookup.Result<SdmScene> result = null;
    private SdmScene scene = null;

    private ConnectionTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(ConnectionTopComponent.class, "CTL_ConnectionTopComponent"));
        setToolTipText(NbBundle.getMessage(ConnectionTopComponent.class, "HINT_ConnectionTopComponent"));
        setIcon(Utilities.loadImage(ICON_PATH, true));
        Preferences pref = NbPreferences.forModule(OptionPanel.class);
        pref.addPreferenceChangeListener(new PreferenceChangeListener() {

            public void preferenceChange(final PreferenceChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {
                        reconnect();
                    }
                });

            }
        });
    }

    public void resultChanged(LookupEvent lookupEvent) {
        Lookup.Result<? extends SdmScene> r = (Lookup.Result<? extends SdmScene>) lookupEvent.getSource();
        Collection<? extends SdmScene> c = r.allInstances();
        if (!c.isEmpty()) {
            SdmScene o = c.iterator().next();
        }
    }

    private SdmScene getSdmScene() {
        if (scene == null) {
            Lookup.Template<SdmScene> tpl = new Lookup.Template(SdmSceneImpl.class);
            result = Utilities.actionsGlobalContext().lookup(tpl);
            result.allInstances().iterator().next();
            scene = Lookup.getDefault().lookup(SdmSceneImpl.class);
            scene = Utilities.actionsGlobalContext().lookup(SdmSceneImpl.class);
        }
        if (scene == null) {
            throw new IllegalStateException("Scene does not exists!");
        }
        return scene;
    }

    private void reconnect() {

        LoggerTopComponent.findInstance().enableNotificationListeners(false);
        SdmSceneTopComponent.findInstance().getSdmScene().clearScene();
        try {
            /** 
             * Root stuff
             */
            RootNode rn = RootNode.getInstance(ExecutionEnvWrapper.getInstance());
            RootWidget rw = (RootWidget) SdmSceneTopComponent.findInstance().getSdmScene().addNode(rn);
            /**
             * end of Root stuff
             */
            /** 
             * RP stuff
             */
            try {
                ResourceProvider rp = getResourceProvider();
                ResourceProviderNode rpn = new ResourceProviderNode(Children.create(new ResourceProviderChildFactory(rp), false), rp);
                Widget w = SdmSceneTopComponent.findInstance().getSdmScene().addNode(rpn);
                if (w != null) {
                    SdmSceneTopComponent.findInstance().getSdmScene().validate();
                    RootEdge sre = new RootEdge(Children.LEAF, rw, w);
                    @SuppressWarnings("unchecked")
                    Widget e = SdmSceneTopComponent.findInstance().getSdmScene().addEdge(sre);
                    SdmSceneTopComponent.findInstance().getSdmScene().validate();
                    if (e != null) {
                        SdmSceneTopComponent.findInstance().getSdmScene().setEdgeSource(sre, rn);
                        SdmSceneTopComponent.findInstance().getSdmScene().setEdgeTarget(sre, rpn);
                    }
                    SdmSceneTopComponent.findInstance().getSdmScene().validate();
                }
            } catch (Exception e) {
                Exceptions.printStackTrace(e);
            }
            /**
             * end of RP
             */
            /** 
             * Service stuff
             */
            try {
                List<Service> serviceList = ComponentService.<Service>getComponentsByType(ExecutionEnvWrapper.getInstance(), Service.class);
                for (Service s : serviceList) {
                    ServiceNode sn = new ServiceNode(Children.create(new ServiceChildFactory(s), false), s);
                    ServiceWidget sw = (ServiceWidget) SdmSceneTopComponent.findInstance().getSdmScene().addNode(sn);
                    if (sw != null) {
                        SdmSceneTopComponent.findInstance().getSdmScene().validate();
                        RootEdge sre = new RootEdge(Children.LEAF, rw, sw);
                        @SuppressWarnings("unchecked")
                        Widget e = SdmSceneTopComponent.findInstance().getSdmScene().addEdge(sre);
                        SdmSceneTopComponent.findInstance().getSdmScene().validate();
                        if (e != null) {
                            SdmSceneTopComponent.findInstance().getSdmScene().setEdgeSource(sre, rn);
                            SdmSceneTopComponent.findInstance().getSdmScene().setEdgeTarget(sre, sn);
                        }
                        SdmSceneTopComponent.findInstance().getSdmScene().validate();
                    }
                }
                LoggerTopComponent.findInstance().enableNotificationListeners(true);
                SdmSceneTopComponent.findInstance().getSdmScene().validate();
                RootChildFactory.getInstance().refresh();
            /**
             * end of service
             */
            } catch (GrmException ex) {
                Exceptions.printStackTrace(ex);
            }
        } catch (GrmException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnRefresh = new javax.swing.JButton();
        btnSceneLayout = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setName(""); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 295, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 260, Short.MAX_VALUE)
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Control panel"));

        org.openide.awt.Mnemonics.setLocalizedText(btnRefresh, "Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(btnSceneLayout, "Scene Layout");
        btnSceneLayout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSceneLayoutActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnRefresh, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnSceneLayout, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
                .add(14, 14, 14))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(btnRefresh, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnSceneLayout, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel2, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents
    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        reconnect();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnSceneLayoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSceneLayoutActionPerformed
        SdmSceneTopComponent.findInstance().getSdmScene().performLayout();
}//GEN-LAST:event_btnSceneLayoutActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSceneLayout;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link findInstance}.
     */
    public static synchronized ConnectionTopComponent getDefault() {
        if (instance == null) {
            instance = new ConnectionTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the ConnectionTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized ConnectionTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(ConnectionTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof ConnectionTopComponent) {
            return (ConnectionTopComponent) win;
        }
        Logger.getLogger(ConnectionTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID +
                "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        Lookup.Template<SdmScene> tpl = new Lookup.Template(SdmScene.class);
        result = Utilities.actionsGlobalContext().lookup(tpl);
        result.addLookupListener(this);
    }

    @Override
    public void componentClosed() {
        result.removeLookupListener(this);
    }

    /** replaces this in object stream */
    @Override
    public Object writeReplace() {
        return new ResolvableHelper();
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    final static class ResolvableHelper implements Serializable {

        private static final long serialVersionUID = 1L;

        public Object readResolve() {
            return ConnectionTopComponent.getDefault();
        }
    }

    private ResourceProvider getResourceProvider() throws GrmException {
        return ComponentService.<ResourceProvider>getComponentByType(ExecutionEnvWrapper.getInstance(), ResourceProvider.class);
    }
}
