/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.connection;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.naming.ConfigurationObjectNotFoundException;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectAddedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectChangedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectRemovedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.service.*;
import com.sun.grid.grm.service.event.*;
import com.sun.grid.grm.service.impl.ServiceCachingProxy;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.FileFilter;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class NodeManager {

//    private final ExecutionEnv env;
//    private final ConfigurationService cs;
    private final Lock lock = new ReentrantLock();
//    private final ConfigurationServiceEventListener csEventListener;
//    private final ServiceEventListener serviceEventListener;
    private boolean started;

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private static final Logger log = Logger.getLogger(NodeManager.class.getName(), BUNDLE);
    
    /**
     * A constructor.
     *
     * @param env Execution enviroment
     * @param serviceStore Instance of ServiceStore, can not be null.
     * @param resourceManager Instance of ResourceManager, can not be null.
     * @throws com.sun.grid.grm.GrmException in any problem during
     * the initial communication with CS
     */
    public NodeManager() throws GrmException {
//            env = ExecutionEnvWrapper.getInstance();
//            cs = ComponentService.getCS(env);
//            csEventListener = new SMConfigurationServiceEventListener();
            started = false;
        
//            this.serviceEventListener = new SMServiceEventListener();
//            this.csEventListener = new SMConfigurationServiceEventListener();        
    }

//    /* helper method for syncing nodes */
//    private void getNodesFromCS() {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "getServicesFromCS");
//        }
//        ResourceProvider rp = ComponentService.<ResourceProvider>getComponentByType(ExecutionEnvWrapper.getInstance(), ResourceProvider.class);
//        
//        List<ComponentInfo> cis = Collections.<ComponentInfo>emptyList();
//        try {
//            cis = ComponentService.getComponentInfosByType(env, Service.class);
//        } catch (Exception e) {
//            if (log.isLoggable(Level.WARNING)) {
//                log.log(Level.WARNING, "asm.gsfcs.cs", e);
//            }
//        }
//        for (ComponentInfo ci : cis) {
//            String serviceName = ci.getName();
//            if (serviceName == null || serviceName.length() == 0) {
//                if (log.isLoggable(Level.WARNING)) {
//                    log.log(Level.WARNING, "asm.error.null_svc_name");
//                }
//            } else {
//                try {
//                    addService(createManagedService(serviceName));
//                } catch (InvalidServiceException ex) {
//                    if (log.isLoggable(Level.WARNING)) {
//                        log.log(Level.WARNING, "asm.gsfcs.add", ex);
//                    }
//                }
//            }
//        }
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "getServicesFromCS");
//        }
//    }
//    
//    /**
//     * Adds a caching proxy for the service to the list of managed services.
//     *
//     * @param service caching proxy for the service
//     * @throws InvalidServiceException thrown when caching proxy can not be added
//     * @return true if caching proxy was added
//     */
//    public boolean addService(ServiceCachingProxy service)
//            throws InvalidServiceException {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "addService", service);
//        }
//        boolean service_added = false;
//        lock.lock();
//        try {
//            service_added = super.addService(service);
//            if (service_added) {
//                service.addServiceEventListener(serviceEventListener);
//            } else {
//                service.destroy();
//            }
//            if (log.isLoggable(Level.FINER)) {
//                log.log(Level.FINER, "scpm.as.ok", service);
//            }
//        } finally {
//            lock.unlock();
//        }
//        if (service_added) {
//            resourceManager.triggerRequestQueueReprocessing();
//        }
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "addService", service_added);
//        }
//        return service_added;
//    }
//
//    /**
//     * Removes a caching proxy from the service manager.  This method has
//     * no direct effect on the information stored in CS or on the removed service.
//     *
//     * @param serviceName name of the caching proxy
//     */
//    @Override
//    public boolean removeService(String serviceName) throws UnknownServiceException {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "removeService", serviceName);
//        }
//        boolean service_removed = false;
//        lock.lock();
//        try {
//            ServiceCachingProxy service = getService(serviceName);
//            service.removeServiceEventListener(serviceEventListener);
//            service_removed = super.removeService(serviceName);
//            if (service_removed) {
//                service.destroy();
//            }
//        } finally {
//            lock.unlock();
//        }
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "removeService", service_removed);
//        }
//        return service_removed;
//    }    
//
//    /**
//     * Starts the node manager.
//     */
//    public void start() {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "start");
//        }
//
//        lock.lock();
//        try {
//            cs.addConfigurationServiceEventListener(csEventListener);
//            super.start();
//        } finally {
//            lock.unlock();
//        }
//
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "start");
//        }
//    }
//
//    /**
//     * Stops the node manager.
//     */
//    public void stop() {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "stop");
//        }
//
//        lock.lock();
//        try {
//            cs.removeConfigurationServiceEventListener(csEventListener);
//            super.stop();
//        } finally {
//            lock.unlock();
//        }
//
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "stop");
//        }
//    }
//
//    /* helper class taking care of service events */
//    private class SMServiceEventListener extends ServiceEventAdapter {
//
//        @Override
//        public void resourceRequest(ResourceRequestEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourcerequest", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceRequestEvent(event);
//            }
//        }
//
//        @Override
//        public void addResource(AddResourceEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.addresource", event);
//            }
//            if (isStarted()) {
//                resourceManager.processAddResourceEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void resourceAdded(ResourceAddedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourceadded", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceAddedEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void removeResource(RemoveResourceEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.removeresource", event);
//            }
//            if (isStarted()) {
//                resourceManager.processRemoveResourceEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void resourceRemoved(ResourceRemovedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourceremoved", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceRemovedEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void resourceRejected(ResourceRejectedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourcerejected", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceRejectedEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void resourceError(ResourceErrorEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourceerror", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceErrorEvent(event.getServiceName(), event.getResource(), event.getMessage());
//            }
//        }
//
//        @Override
//        public void resourceReset(ResourceResetEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourcereset", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourceResetEvent(event.getServiceName(), event.getResource());
//            }
//        }
//
//        @Override
//        public void resourceChanged(ResourceChangedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.resourcepropchanged", event);
//            }
//            if (isStarted()) {
//                resourceManager.processResourcePropertiesChangedEvent(event.getServiceName(), event.getResource(), event.getProperties());
//            }
//        }
//
//        @Override
//        public void serviceRunning(ServiceStateChangedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.servicerunning", event);
//            }
//            if (isStarted()) {
//                resourceManager.triggerRequestQueueReprocessing();
//            }
//        }
//
//        @Override
//        public void serviceStopped(ServiceStateChangedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.servicestopped", event);
//            }
//            if (isStarted()) {
//                resourceManager.purgeOrders(event.getServiceName());
//                resourceManager.purgeRequests(event.getServiceName());
//            }
//        }
//    }
//
//    /* helper method for removing SCP spool of not existing services */
//    private void removeObsoleteSCPSpools() {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "removeObsoleteSCPSpools");
//        }
//        /* get list of directiores where SCPs are spooling cached resources */
//        File[] scpSpoolDirs = PathUtil.getRPSpoolPath(env).listFiles(new FileFilter() {
//
//            public boolean accept(File pathname) {
//                return pathname.isDirectory();
//            }
//        });
//        if (scpSpoolDirs != null) {
//            for (File f : scpSpoolDirs) {
//                GetConfigurationCommand<ComponentConfig> gcc = new GetConfigurationCommand<ComponentConfig>();
//                /* name of the scp spool dir is the same as service name and thus the same
//                 * as the service's component's config name */
//                gcc.setConfigName(f.getName());
//                ComponentConfig cc = null;
//                try {
//                    cc = env.getCommandService().execute(gcc).getReturnValue();
//                } catch (ConfigurationObjectNotFoundException conf) {
//                    if (log.isLoggable(Level.FINER)) {
//                        log.log(Level.FINER, "scpm.rosss.conf", f.getName());
//                    }
//                } catch (GrmException ex) {
//                    if (log.isLoggable(Level.SEVERE)) {
//                        log.log(Level.SEVERE, "scpm.rosss.cogrexa", new Object[]{f.getName(), ex});
//                    }
//                    /* well, what to do .. skip or continue? ask in review */
//                    continue;
//                /* by continueing interation, we actually skipp removing of the spool */
//                }
//                /* no component configuration with service's name was found, it is 
//                 * almost safe to remove SCP spool dir - we just need to be sure that 
//                 * SCP was already destroyed, but that is on*/
//                if (cc == null) {
//                    removeObsoleteSCPSpool(f.getName());
//                } else {
//                    if (log.isLoggable(Level.FINER)) {
//                        log.log(Level.FINER, "scpm.rosss.cosi", f.getName());
//                    }
//                }
//            }
//        }
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "removeObsoleteSCPSpools");
//        }
//    }
//
//    /* before calling this method we have to be sure that component configuration
//     * for the service was removed - this method is not doing that check */
//    private void removeObsoleteSCPSpool(String serviceName) {
//        if (log.isLoggable(Level.FINER)) {
//            log.entering(NodeManager.class.getName(), "removeObsoleteSCPSpool", serviceName);
//        }
//        lock.lock();
//        try {
//            Service scp = null;
//            try {
//                scp = getService(serviceName);
//            } catch (UnknownServiceException ex) {
//                if (log.isLoggable(Level.FINER)) {
//                    log.log(Level.FINER, "scpm.ross.uscp", serviceName);
//                }
//            }
//            if (scp == null) {
//                /* we are definitely safe to remove */
//                Platform.getPlatform().removeDir(PathUtil.getRPSpoolDirForComponent(env, serviceName), true);
//                if (log.isLoggable(Level.FINER)) {
//                    log.log(Level.FINER, "scpm.ross.scp_rem_ok", serviceName);
//                }
//            } else {
//                /* SCP is still in memory, leave cleanup for SCP.destroy() */
//                if (log.isLoggable(Level.FINER)) {
//                    log.log(Level.FINER, "scpm.ross.scp_inmem", serviceName);
//                }
//            }
//        } catch (Exception ex) {
//            if (log.isLoggable(Level.SEVERE)) {
//                log.log(Level.SEVERE, "scpm.ross.scp_rem_fail", new Object[]{serviceName, ex});
//            }
//        } finally {
//            lock.unlock();
//        }
//        if (log.isLoggable(Level.FINER)) {
//            log.exiting(NodeManager.class.getName(), "removeObsoleteSCPCache");
//        }
//    }
//
//    /* helper class taking care of CS events */
//    private class SMConfigurationServiceEventListener
//            implements ConfigurationServiceEventListener {
//
//        public void configurationObjectAdded(ConfigurationObjectAddedEvent event) {
//            /* do not have to do anything */
//        }
//
//        public void configurationObjectRemoved(ConfigurationObjectRemovedEvent event) {
//            if (log.isLoggable(Level.FINE)) {
//                log.log(Level.FINE, "scpm.event.cor", event);
//            }
//            if (isStarted() && event.getContextPath().equals(BootstrapConstants.CS_COMPONENT)) {
//                /* just got event that ComponentConfig was removed, so it is safe
//                 * to remove related SCP spool dir */
//                removeObsoleteSCPSpool(event.getName());
//                /* and remove also requests and orders */
//                resourceManager.purgeOrders(event.getName());
//                resourceManager.purgeRequests(event.getName());
//            }
//        }
//
//        public void configurationObjectChanged(ConfigurationObjectChangedEvent event) {
//            /* do not have to do anything */
//        }
//    }
}
