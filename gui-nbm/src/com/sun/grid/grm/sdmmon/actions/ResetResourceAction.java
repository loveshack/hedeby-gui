/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.grid.grm.sdmmon.actions;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.impl.AnyResourceIdImpl;
import com.sun.grid.grm.sdmmon.node.ResourceNode;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.ui.resource.ResetResourceCommand;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public final class ResetResourceAction extends CookieAction {

    private static final Logger log = Logger.getLogger(ResetResourceAction.class.getName());
    private static final long serialVersionUID = -2008012501L;

    protected void performAction(Node[] activatedNodes) {
        for (int i = 0; i < activatedNodes.length; i++) {
            ResourceNode resourceNode = activatedNodes[i].getLookup().lookup(ResourceNode.class);
            if (resourceNode != null) {
                try {
                    ResourceId id = new AnyResourceIdImpl(resourceNode.getName());
                    ResetResourceCommand rrc = new ResetResourceCommand(Collections.singletonList(id), resourceNode.getOwnerName());
                    ExecutionEnvWrapper.getInstance().getCommandService().execute(rrc);
                } catch (GrmException ex) {
                    String msg = "failed to reset a resource: " + resourceNode.getName();
                    int msgType = NotifyDescriptor.INFORMATION_MESSAGE;
                    NotifyDescriptor d = new NotifyDescriptor.Message(msg, msgType);
                    DialogDisplayer.getDefault().notify(d);
                    log.log(Level.WARNING, "failed to reset a resource: " + resourceNode.getName(), ex);
                }
            } else {
                log.log(Level.WARNING, "failed to reset a resource, activated resource node is null");
            }
        }

    }

    protected int mode() {
        return CookieAction.MODE_ALL;
    }

    public String getName() {
        return NbBundle.getMessage(ResetResourceAction.class, "CTL_ResetResourceAction");
    }

    protected Class[] cookieClasses() {
        return new Class[]{ResourceNode.class};
    }

    @Override
    protected String iconResource() {
        return "com/sun/grid/grm/sdmmon/img/139.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

