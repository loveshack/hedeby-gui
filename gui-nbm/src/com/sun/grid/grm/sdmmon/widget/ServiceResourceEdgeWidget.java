/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.widget;

import com.sun.grid.grm.sdmmon.edge.ServiceResourceEdge;
import com.sun.grid.grm.sdmmon.scene.SdmScene;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.PointShape;
import org.netbeans.api.visual.widget.ConnectionWidget;

public class ServiceResourceEdgeWidget extends ConnectionWidget {

    private ServiceResourceEdge node;
    
    public ServiceResourceEdgeWidget(SdmScene scene, ServiceResourceEdge node) {
        super(scene);
        this.node = node;
        this.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
        this.setEndPointShape(PointShape.SQUARE_FILLED_BIG);
//        this.getActions().addAction(createObjectHoverAction());
//        this.getActions().addAction(createSelectAction());
    }

    public ServiceResourceEdge getNode() {
        return this.node;
    }    
}
