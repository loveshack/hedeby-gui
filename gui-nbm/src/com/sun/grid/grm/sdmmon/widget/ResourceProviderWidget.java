/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.widget;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.impl.AnyResourceIdImpl;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.sdmmon.edge.RPResourceEdge;
import com.sun.grid.grm.sdmmon.node.ResourceNode;
import com.sun.grid.grm.sdmmon.node.ResourceProviderNode;
import com.sun.grid.grm.sdmmon.scene.SdmScene;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import java.awt.Image;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Utilities;

public class ResourceProviderWidget extends NodeWidget {

    private static final Logger log = Logger.getLogger(ResourceProviderWidget.class.getName());
    private final Map<ResourceId, ResourceNode> resourceNodez = new HashMap<ResourceId, ResourceNode>();
    private static final String ICON_PATH = "com/sun/grid/grm/sdmmon/widget/img/Crystal_Clear_app_agent.png";
    private static final Image image = Utilities.loadImage(ICON_PATH, true);
    private final ResourceProviderEventListener myrpel = new RPWResourceProviderEventListener();
    private final Lock actionLock = new ReentrantLock();
    private long lastReceivedComponentEvent = Long.MIN_VALUE;
    private long lastReceivedRPEvent = Long.MIN_VALUE;

    public ResourceProviderWidget(SdmScene scene, ResourceProviderNode node) {
        super(scene, node);
        this.setImage(image.getScaledInstance(48, 48, Image.SCALE_AREA_AVERAGING));
        try {
            node.getResourceProvider().addResourceProviderEventListener(myrpel);
            this.getLabelWidget().setFont(this.getLabelWidget().getFont().deriveFont(2, 11f));
            this.updateLabel();
            this.revalidate();
            getScene().validate();
            refresh();
        } catch (Exception ex) {
            log.log(Level.WARNING, "failed to create a rp widget for rp node: " + node.getName(), ex);
        }
    }

    @Override
    public ResourceProviderNode getNode() {
        return (ResourceProviderNode) super.getNode();
    }

    @Override
    public void updateLabel() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getLabelWidget().setLabel(getNode().getDisplayName());
                getScene().validate();
            }
        });

    }

    private class RPWResourceProviderEventListener implements ResourceProviderEventListener {

        @Override
        public boolean equals(Object o) {
            if (o instanceof RPWResourceProviderEventListener) {
                if (getName() != null) {
                    return getName().equals(((RPWResourceProviderEventListener) o).getName());
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 73 * hash + (getName() != null ? getName().hashCode() : 0);
            return hash;
        }

        public String getName() {
            return ResourceProviderWidget.this.getNode().getDisplayName();
        }

        public void addResource(final AddResourceEvent event) {
            addAction(event);
        }

        public void resourceAdded(final ResourceAddedEvent event) {
            addAction(event);
        }

        public void removeResource(final RemoveResourceEvent event) {
            addAction(event);
        }

        public void resourceRemoved(final ResourceRemovedEvent event) {
            addAction(event);
        }

        public void resourceError(final ResourceErrorEvent event) {
            addAction(event);
        }

        public void resourceReset(final ResourceResetEvent event) {
            addAction(event);
        }

        public void resourceChanged(final ResourceChangedEvent event) {
            addAction(event);
        }

        public void resourceRejected(final ResourceRejectedEvent event) {
            addAction(event);
        }
    }

    /* helper interface - for command pattern like processing of resource events */
    private abstract class Action implements Runnable {

        protected ResourceNode n;

        public Action(ResourceNode n) {
            this.n = n;
        }

        public abstract boolean execute();

        public final void run() {
            execute();
        }
    }

    /**
     * helper method that checks whether the service event is not doubled or is 
     * not received after and event with newer ID 
     *
     */
    private boolean isRPEventNew(AbstractServiceEvent e) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceWidget.class.getName(), "isRPEventNew", e);
        }
        boolean ret = false;
        if (lastReceivedRPEvent < e.getSequenceNumber()) {
            if ((e.getSequenceNumber() - lastReceivedRPEvent) > 1) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "Some events skipped, need to do full refresh", new Object[]{getNode(), lastReceivedRPEvent, e.getSequenceNumber()});
                }
            }
            lastReceivedRPEvent = e.getSequenceNumber();
            ret = true;
        } else if (log.isLoggable(Level.WARNING)) {
            log.log(Level.WARNING, "Event is old, will be skipped", new Object[]{getNode(), lastReceivedRPEvent, e.getSequenceNumber(), e});
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ServiceWidget.class.getName(), "isRPEventNew", ret);
        }
        return ret;
    }

    /* helper method for submitting an action triggered by a service event */
    private void addAction(AbstractServiceEvent e) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceWidget.class.getName(), "addAction", e);
        }
        actionLock.lock();
        try {
            if (isRPEventNew(e)) {
                refresh();
            }
        } finally {
            actionLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ServiceWidget.class.getName(), "addAction");
        }
    }

    /* encapsulates processing of resource event that causes a refresh of all resources*/
    private void refresh() {

        updateLabel();

        Collection<ResourceNode> toAdd = new HashSet<ResourceNode>();
        Collection<ResourceNode> toUpdate = new HashSet<ResourceNode>();
        Collection<ResourceNode> toRemove = new HashSet<ResourceNode>();
        Collection<ResourceId> actual = resourceNodez.keySet();

        Node[] nodes = getNode().getChildren().getNodes();
        Map<ResourceId, ResourceNode> newNodez = new HashMap<ResourceId, ResourceNode>(nodes.length);
        for (int i = 0; i < nodes.length; i++) {
            ResourceNode rn = (ResourceNode) nodes[i];
            newNodez.put(rn.getResource().getId(), rn);
        }
        for (Map.Entry<ResourceId, ResourceNode> entry : newNodez.entrySet()) {
            if (!actual.contains(entry.getKey())) {
                toAdd.add(entry.getValue());
            } else {
                toUpdate.add(entry.getValue());
            }
        }
        for (ResourceId n : actual) {
            if (!newNodez.keySet().contains(n)) {
                toRemove.add(resourceNodez.get(n));
            }
        }
        log.log(Level.INFO, "refresh action will add resources   : " + toAdd);
        log.log(Level.INFO, "refresh action will remove resources: " + toRemove);
        log.log(Level.INFO, "refresh action will update resources: " + toUpdate);

        for (final ResourceNode n : toAdd) {
            SwingUtilities.invokeLater(new AddResourceAction(n));
        }

        for (final ResourceNode n : toUpdate) {
            SwingUtilities.invokeLater(new UpdateResourceAction(n));
        }

        for (final ResourceNode n : toRemove) {
            SwingUtilities.invokeLater(new RemoveResourceAction(n));
        }
    }

    private final class UpdateResourceAction extends Action {

        public UpdateResourceAction(ResourceNode n) {
            super(n);
        }

        @Override
        public boolean execute() {
            log.log(Level.INFO, "entering update resource action, rp: " + getNode().getName() + ", resource:" + n.getName());
            @SuppressWarnings("unchecked")
            ResourceNode w = resourceNodez.get(n);
            if (getSdmScene().getNodes().contains(w)) {
                w.update(n);
            }
            log.log(Level.INFO, "exiting update resource action, rp: " + getNode().getName() + ", resource:" + n.getName());
            return true;
        }
    }

    private final class RemoveResourceAction extends Action {

        public RemoveResourceAction(ResourceNode n) {
            super(n);
        }

        @Override
        public boolean execute() {
            if (getNode() == null) {
                log.log(Level.WARNING, "getNode null");
            }
            if (n == null) {
                log.log(Level.WARNING, "n null");
            }
            log.log(Level.INFO, "entering remove resource action, rp: " + getNode().getName() + ", resource:" + n.getName());

            resourceNodez.remove(n);
            if (getSdmScene().getNodes().contains(n)) {
                log.log(Level.INFO, "rp: " + getNode().getName() + ", has resource:" + n.getName() + " on the scene, removing it with its edges");
                
                getSdmScene().removeNodeWithEdges(n);
                updateScene();
            } else {
                log.log(Level.INFO, "rp: " + getNode().getName() + ", does not have resource:" + n.getName() + " on the scene, no need to remove");
            }
            log.log(Level.INFO, "exiting remove resource action, rp: " + getNode().getName() + ", resource:" + n.getName());

            return true;
        }
    }

    private final class AddResourceAction extends Action {

        public AddResourceAction(ResourceNode n) {
            super(n);
        }

        @Override
        public boolean execute() {
            log.log(Level.INFO, "entering add resource action, rp: " + getNode().getName() + ", resource:" + n.getName());

            ResourceId id = new AnyResourceIdImpl(n.getName());
            if (!resourceNodez.containsKey(id)) {
                log.log(Level.INFO, "service: " + getNode().getName() + " does not yet have resource:" + n.getName() + " on the scene, will be added with edges");
                Widget w = getSdmScene().addNode(n);
                if (w != null) {
                    resourceNodez.put(id, n);
                    RPResourceEdge sre = new RPResourceEdge(Children.LEAF, ResourceProviderWidget.this, (ResourceWidget) w);
                    @SuppressWarnings("unchecked")
                    Widget e = getSdmScene().addEdge(sre);
                    if (e != null) {
                        if (!getSdmScene().getNodes().contains(ResourceProviderWidget.this.getNode())) {
                            log.log(Level.WARNING, "rp node is not part of graph: " + getNode().getName() + ", something is messed up");
                        } else {
                            getSdmScene().setEdgeSource(sre, ResourceProviderWidget.this.getNode());
                            getSdmScene().setEdgeTarget(sre, n);
                        }
                    }
                }
            } else {
                log.log(Level.INFO, "rp: " + getNode().getName() + " already has resource:" + n.getName() + " on the scene, no need to add (should be updated ..)");
            }
            updateScene();
            log.log(Level.INFO, "exiting add resource action, rp: " + getNode().getName() + ", resource:" + n.getName());
            return true;
        }
    }
}
