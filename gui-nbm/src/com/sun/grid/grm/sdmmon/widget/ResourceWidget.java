/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.widget;

import com.sun.grid.grm.sdmmon.node.ResourceNode;
import com.sun.grid.grm.sdmmon.scene.SdmScene;
import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.border.BorderFactory;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Utilities;

public class ResourceWidget extends NodeWidget {

    private static final Logger log = Logger.getLogger(ResourceWidget.class.getName());
    private static final String ICON_PATH = "com/sun/grid/grm/sdmmon/widget/img/Crystal_Clear_app_mycomputer.png";
    private static final Image image = Utilities.loadImage(ICON_PATH, true);
    private final NodeListener nl = new MyNodeListener();

    public ResourceWidget(SdmScene scene, ResourceNode node) {
        super(scene, node);
        this.getNode().addNodeListener(nl);
        this.setImage(image.getScaledInstance(32, 32, Image.SCALE_AREA_AVERAGING));
        this.getLabelWidget().setVisible(true);
        this.getLabelWidget().setFont(this.getLabelWidget().getFont().deriveFont(1, 9f));
        this.getImageWidget().setOpaque(false);
        this.setOpaque(false);
        updateLabel();
    }

    @Override
    public void updateLabel() {
        switch (getNode().getResource().getState()) {
            case ASSIGNED: {
                updateLabel(Color.GREEN);
                break;
            }
            case ASSIGNING: {
                updateLabel(Color.YELLOW);
                break;
            }
            case UNASSIGNING: {
                updateLabel(Color.WHITE);
                break;
            }
            case UNASSIGNED: {
                updateLabel(Color.LIGHT_GRAY);
                break;
            }
            case INPROCESS: {
                updateLabel(Color.DARK_GRAY);
                break;
            }
            case ERROR: {
                updateLabel(Color.RED);
                break;
            }
            default:
                updateLabel(Color.BLUE);
                break;
        }
        showBorder(getNode().getResource().isAmbiguous());
    }

    @Override
    public ResourceNode getNode() {
        return (ResourceNode) super.getNode();
    }

    @Override
    protected void updateLabel(final Color color) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getLabelWidget().setLabel(getNode().getDisplayName());
                getScene().getSceneAnimator().animateBackgroundColor(getLabelWidget(), color);
            }
        });
    }

    private void showBorder(final boolean show) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                if (show) {
                    getLabelWidget().setBorder(BorderFactory.createLineBorder(2, Color.BLACK));
                } else {
                    getLabelWidget().setBorder(BorderFactory.createEmptyBorder());
                }
            }
        });
    }

    private class MyNodeListener implements NodeListener {

        public void childrenAdded(NodeMemberEvent arg0) {
            // ResourceNode has no children
        }

        public void childrenRemoved(NodeMemberEvent arg0) {
            // ResourceNode has no children
        }

        public void childrenReordered(NodeReorderEvent arg0) {
            // ResourceNode has no children
        }

        public void nodeDestroyed(NodeEvent arg0) {
            log.log(Level.INFO, "entering nodeDestroyed", arg0);
            getScene().getSceneAnimator().animatePreferredBounds(ResourceWidget.this, new Rectangle(0, 0));
            log.log(Level.INFO, "exiting nodeDestroyed");
        }

        public void propertyChange(PropertyChangeEvent evt) {
            log.log(Level.INFO, "entering propertyChange", evt);
            updateLabel();
            log.log(Level.INFO, "exiting propertyChange");
        }
    }
}
