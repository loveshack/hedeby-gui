/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.edge;

import com.sun.grid.grm.sdmmon.*;
import com.sun.grid.grm.sdmmon.widget.ResourceProviderWidget;
import com.sun.grid.grm.sdmmon.widget.ResourceWidget;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class RPResourceEdge extends AbstractNode {

    private ResourceProviderWidget source;
    private ResourceWidget target;

    public RPResourceEdge(Children c, ResourceProviderWidget source, ResourceWidget target) {
        super(c);
        this.source = source;
        this.target = target;
    }

    public RPResourceEdge(Children c, Lookup l, ResourceProviderWidget source, ResourceWidget target) {
        super(c, l);
        this.source = source;
        this.target = target;
    }

    public ResourceProviderWidget getSource() {
        return source;
    }

    public ResourceWidget getTarget() {
        return target;
    }

    @Override
    public String getDisplayName() {
        return source.getNode().getDisplayName() + "_" + target.getNode().getDisplayName();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            @SuppressWarnings("unchecked")
            Property nameProp = new PropertySupport.Reflection(this, String.class,
                    "getDisplayName", null);

            nameProp.setName("name");
            set.put(nameProp);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "error during property sheet creation, no such method", ex);
        }
        sheet.put(set);
        return sheet;
    }
}
