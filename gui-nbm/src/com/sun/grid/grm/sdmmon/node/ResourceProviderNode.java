/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.ui.component.ReloadComponentCommand;
import com.sun.grid.grm.ui.component.StartComponentCommand;
import com.sun.grid.grm.ui.component.StopComponentCommand;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class ResourceProviderNode extends AbstractNode {

    private ResourceProvider rp;
    private final Action start = new StartAction();
    private final Action stop = new StopAction();
    private final Action reload = new ReloadAction();
    private final String name;

    public ResourceProviderNode(Children c, ResourceProvider rp) {
        super(c);
        if (rp == null) {
            throw new IllegalArgumentException("ResourceProvider must not be null");
        }
        this.rp = rp;
        String rpName = "resource_provider";
        try {
            rpName = this.rp.getName();
        } catch (GrmRemoteException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "failed to get rp name, using 'resource_provider'", ex);
            throw new IllegalArgumentException("ResourceProvider name can not be retrieved");
        }
        this.name = rpName;
    }

    public ResourceProviderNode(Children c, Lookup l, ResourceProvider rp) {
        super(c, l);
        this.rp = rp;
        String rpName = "resource_provider";
        try {
            rpName = this.rp.getName();
        } catch (GrmRemoteException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "failed to get rp name, using 'resource_provider'", ex);
        }
        this.name = rpName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceProviderNode other = (ResourceProviderNode) obj;
        if (this.name == null || !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public void setResourceProvider(ResourceProvider rp) {
        this.rp = rp;
        this.setChildren(Children.create(new ResourceProviderChildFactory(rp), false));
    }

    public ResourceProvider getResourceProvider() {
        return rp;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public String getHtmlDisplayName() {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='000000'><b>");
        sb.append(getDisplayName());
        sb.append("</b></font>");
        return sb.toString();
    }

    @Override
    public Node cloneNode() {
        return new ResourceProviderNode(this.getChildren(), this.getLookup(), this.rp);
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            @SuppressWarnings("unchecked")
            Property nameProp = new PropertySupport.Reflection(rp, String.class,
                    "getName", null);

            nameProp.setName(
                    "name");
            set.put(nameProp);
            ComponentState s = ComponentState.UNKNOWN;
            try {
                s = rp.getState();
            } catch (GrmRemoteException grexa) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "failed to get rp state, using 'UNKNOWN'", grexa);
            }
            Property prop = new ReadOnlyProperty("state", s.toString(), "state", "state");
            set.put(prop);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "error in property shhet creation, no such method", ex);
        }

        sheet.put(set);
        return sheet;
    }

    private class ReadOnlyProperty
            extends PropertySupport.ReadOnly<String> {

        private final String value;

        private ReadOnlyProperty(String name, String value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return value;
        }
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{reload, start, stop};
    }

    private class ReloadAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public ReloadAction() {
            putValue(NAME, "Reload");
        }

        public void actionPerformed(final ActionEvent e) {
            try {
                ReloadComponentCommand rcc = new ReloadComponentCommand();
                rcc.setComponentName(getName());
                ExecutionEnvWrapper.getInstance().getCommandService().execute(rcc);
            } catch (GrmException ex) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, "failed to reload rp", ex);
            }
        }
    }

    private class StartAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public StartAction() {
            putValue(NAME, "Start");
        }

        public void actionPerformed(final ActionEvent e) {
            try {
                StartComponentCommand scc = new StartComponentCommand();
                scc.setComponentName(getName());
                ExecutionEnvWrapper.getInstance().getCommandService().execute(scc);
            } catch (GrmException ex) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, "failed to start rp", ex);
            }
        }
    }

    private class StopAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public StopAction() {
            putValue(NAME, "Stop");
        }

        public void actionPerformed(final ActionEvent e) {
            try {
                StopComponentCommand scc = new StopComponentCommand();
                scc.setComponentName(getName());
                ExecutionEnvWrapper.getInstance().getCommandService().execute(scc);
            } catch (GrmException ex) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, "failed to stop rp", ex);
            }
        }
    }
}
