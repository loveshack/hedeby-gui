/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.impl.AnyResourceIdImpl;
import com.sun.grid.grm.sdmmon.*;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.ui.resource.RemoveResourceCommand;
import com.sun.grid.grm.ui.resource.ResetResourceCommand;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class ResourceNode extends AbstractNode {

    private boolean isRPResource = false;
    private String ownerName;
    private Resource resource;
    private final Action modify = new ModifyAction();
    private final Action remove = new RemoveAction();
    private final Action reset = new ResetAction();
    private static final Logger log = Logger.getLogger(ResourceNode.class.getName());

    public ResourceNode(Children c, Resource r) {
        super(c);
        this.resource = r;
        this.isRPResource = true;
    }

    public ResourceNode(Children c, Resource r, String owner) {
        super(c);
        this.resource = r;
        this.isRPResource = false;
        this.ownerName = owner;
    }

    ResourceNode(Children c, Lookup l, Resource r) {
        super(c, l);
        this.resource = r;
    }

    @Override
    public String getName() {
        return this.resource.getId().getId();
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public String getHtmlDisplayName() {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='222222'>");
        sb.append(getDisplayName());
        sb.append("</font>");
        return sb.toString();
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Resource getResource() {
        return resource;
    }

    public String getOwnerName() {
        return this.ownerName;
    }

    public void update(ResourceNode n) {
        log.log(Level.INFO, "entering update resource node, resource: " + getResource().getId());
        Resource.State oldS = this.getResource().getState();
        Resource.State newS = n.getResource().getState();
        if (!oldS.equals(newS)) {
            log.log(Level.INFO, "resource: " + getResource().getId() + " will be updated, old state was:" + oldS + ", new state is:" + newS);
            this.getResource().setState(newS);
            firePropertyChange("Resource.State", oldS, newS);
        } else {
            log.log(Level.INFO, "resource: " + getResource().getId() + " need not to be updated, state is still the same:" + oldS);
        }
        log.log(Level.INFO, "exiting update resource node, resource: " + getResource().getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.resource != null ? this.resource.hashCode() : 0);
        hash = 37 * hash + (this.ownerName != null ? this.ownerName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceNode other = (ResourceNode) obj;
        if (this.ownerName == null || !this.ownerName.equals(other.ownerName)) {
            return false;
        }
        if (this.resource != other.resource && (this.resource == null || !this.resource.equals(other.resource))) {
            return false;
        }
        return true;
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            ResourceId id = this.resource.getId();
            Property idProp = new PropertySupport.Reflection<String>(id, String.class,
                    "getId", null);

            idProp.setName("id");
            set.put(idProp);
            Property ambProp = new ReadOnlyProperty("isAmbiguous", String.valueOf(this.resource.isAmbiguous()), "isAmbiguous", "isAmbiguous");

            ambProp.setName("isAmbiguous");
            set.put(ambProp);
            Property stateProp = new PropertySupport.Reflection<Resource.State>(this.resource, Resource.State.class,
                    "getState", null);

            stateProp.setName("State");
            set.put(stateProp);
            Map<String, Object> rp = this.resource.getProperties();
            List<String> keys = new ArrayList<String>(rp.keySet());
            for (String key : keys) {
                Property prop = new ReadOnlyProperty(key, rp.get(key), key, key);
                set.put(prop);
            }

        } catch (NoSuchMethodException ex) {
            log.log(Level.WARNING, "error in property sheet creation, no such method", ex);
        }
        sheet.put(set);
        return sheet;
    }

    private class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private final Object value;

        private ReadOnlyProperty(String name, Object value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return value.toString();
        }
    }

    @Override
    public Action[] getActions(boolean popup) {
        return new Action[]{modify, remove, reset};
    }

    private class ResetAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public ResetAction() {
            putValue(NAME, "Reset");
        }

        public void actionPerformed(final ActionEvent e) {
            try {
                ResourceId id = new AnyResourceIdImpl(getName());
                ResetResourceCommand rrc = new ResetResourceCommand(Collections.singletonList(id), ownerName);
                ExecutionEnvWrapper.getInstance().getCommandService().execute(rrc);
            } catch (GrmException ex) {
                log.log(Level.WARNING, "failed to reset a resource: " + getName(), ex);
            }
        }
    }

    private class RemoveAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public RemoveAction() {
            putValue(NAME, "Remove");
        }

        public void actionPerformed(final ActionEvent e) {
            try {
                ResourceId id = new AnyResourceIdImpl(getName());
                RemoveResourceCommand rrc = new RemoveResourceCommand(Collections.singletonList(id), ownerName, false);
                ExecutionEnvWrapper.getInstance().getCommandService().execute(rrc);
            } catch (GrmException ex) {
                log.log(Level.WARNING, "failed to remove a resource: " + getName(), ex);
            }
        }
    }

    private class ModifyAction extends AbstractAction {

        private static final long serialVersionUID = -2008012501L;

        public ModifyAction() {
            putValue(NAME, "Modify");
        }

        public void actionPerformed(final ActionEvent e) {
            JOptionPane.showMessageDialog(null, "Hello from " + e.getSource().toString());
        }
    }

    private class ModifyResourceUI extends javax.swing.JPanel {

        private SortedTableModel tm = new SortedTableModel();
        private boolean modify = false;
        private javax.swing.JButton bntOK;
        private javax.swing.JButton btnCancel;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JTable tblProps;

        /** Creates new form ModifyResourceUI */
        public ModifyResourceUI() {
            initComponents();
        }

        public void showDialog() {
            tm.clear();
            Map<String, Object> rp = resource.getProperties();
            for (Map.Entry<String, Object> entry : rp.entrySet()) {
                tm.addRow(new TableRow(entry.getKey(), entry.getValue()));
            }
            tm.fireTableDataChanged();
        }

        private void initComponents() {

            jScrollPane1 = new javax.swing.JScrollPane();
            tblProps = new javax.swing.JTable();
            bntOK = new javax.swing.JButton();
            btnCancel = new javax.swing.JButton();

            tblProps.setModel(tm);
            jScrollPane1.setViewportView(tblProps);

            bntOK.setText(org.openide.util.NbBundle.getMessage(ModifyResourceUI.class, "ModifyResourceUI.bntOK.text")); // NOI18N

            bntOK.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    bntOKActionPerformed(evt);
                }
            });

            btnCancel.setText(org.openide.util.NbBundle.getMessage(ModifyResourceUI.class, "ModifyResourceUI.btnCancel.text")); // NOI18N

            btnCancel.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });

            org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
            this.setLayout(layout);
            layout.setHorizontalGroup(
                    layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup().addContainerGap(13, Short.MAX_VALUE).add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING).add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 375, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(layout.createSequentialGroup().add(bntOK).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(btnCancel))).addContainerGap()));
            layout.setVerticalGroup(
                    layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup().addContainerGap().add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE).add(18, 18, 18).add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(btnCancel).add(bntOK)).addContainerGap()));
        }

        private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {
            // TODO add your handling code here:
            this.modify = false;
            this.setVisible(false);
        }

        private void bntOKActionPerformed(java.awt.event.ActionEvent evt) {
            // TODO add your handling code here:
            this.modify = true;
            this.setVisible(false);
        }
    }

    private class TableRow {

        private final String p;
        private final Object v;

        TableRow(String aProperty, Object aValue) {
            p = aProperty;
            v = aValue;
        }
    }

    private class SortedTableModel extends AbstractTableModel implements Comparator<Integer> {

        public List<Integer> sortedLines;
        private List<Integer> columnSortOrder;
        private final static long serialVersionUID = -2008012701L;
        private final List<TableRow> rows;

        public SortedTableModel() {
            rows = new LinkedList<TableRow>();
        }

        public void clear() {
            rows.clear();
        }

        public void addRow(TableRow aRow) {
            rows.add(aRow);
            sort();
            fireTableDataChanged();
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 2;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            TableRow row = rows.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return row.p;
                case 1:
                    return row.v;
                default:
                    throw new IllegalArgumentException("Wrong column index!");
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "Property";
                case 1:
                    return "Value";
                default:
                    throw new IllegalArgumentException("Unknown column");
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return String.class;
                default:
                    return String.class;
            }
        }

        public void setColumnSortOrder(List<String> sortedColumns) throws GrmException {
            columnSortOrder = new ArrayList<Integer>(sortedColumns.size());
            for (String colName : sortedColumns) {
                int colIndex = getColumnIndex(colName);
                if (colIndex < 0) {
                    throw new GrmException("AbstractSortedTableCliCommand.uc", AbstractCli.BUNDLE, colName);
                }
                columnSortOrder.add(colIndex);
            }
        }

        public int getColumnIndex(String name) {
            int colCount = getColumnCount();
            for (int i = 0; i < colCount; i++) {
                if (getColumnName(i).equals(name)) {
                    return i;
                }
            }
            return -1;
        }

        private void sort() {
            if (sortedLines == null) {
                int rowCount = getRowCount();
                sortedLines = new ArrayList<Integer>(getRowCount());

                for (int i = 0; i < rowCount; i++) {
                    sortedLines.add(i);
                }
                Collections.sort(sortedLines, this);
            }
        }

        public int compare(Integer line1, Integer line2) {
            int ret = 0;
            int colCount = getColumnCount();
            if (columnSortOrder == null) {
                for (int col = 0; col < colCount && ret == 0; col++) {
                    if (Comparable.class.isAssignableFrom(getColumnClass(col))) {
                        Comparable c1 = (Comparable) getValueAt(line1, col);
                        Comparable c2 = (Comparable) getValueAt(line2, col);
                        ret = compare(c1, c2);
                    }
                }
            } else {
                for (int col : columnSortOrder) {
                    if (Comparable.class.isAssignableFrom(getColumnClass(col))) {
                        Comparable c1 = (Comparable) getValueAt(line1, col);
                        Comparable c2 = (Comparable) getValueAt(line2, col);
                        ret = compare(c1, c2);
                        if (ret != 0) {
                            break;
                        }
                    }
                }
            }
            return ret;
        }

        @SuppressWarnings("unchecked")
        private int compare(Comparable c1, Comparable c2) {
            if (c1 == null) {
                if (c2 != null) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return c1.compareTo(c2);
            }
        }
    }

    @Override
    public String toString() {
        return String.format("[ ResourceNode, name=%s ]", getName());
    }
}
