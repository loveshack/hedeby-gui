/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.impl.AnyResourceIdImpl;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ServiceChildFactory extends ChildFactory<String> {

    private static long counter;
    private final Service service;
    private final ServiceEventListener sel;
    private final ComponentEventListener scl;
    private final Lock lock = new ReentrantLock();

    public ServiceChildFactory(Service s) {
        if (s == null) {
            throw new IllegalArgumentException("Service must not be null");
        }
        this.service = s;
        this.sel = new SCFServiceEventListener();
        this.scl = new SCFComponentEventListener();
        try {
            service.addServiceEventListener(sel);
            service.addComponentEventListener(scl);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "error registering listeners", e);
        }
        counter++;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        arg0.clear();
        if (service != null) {
            lock.lock();
            try {
                List<Resource> resources = service.getResources();
                for (Resource r : resources) {
                    arg0.add(r.getId().getId());
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resources", ex);
            } finally {
                lock.unlock();
            }
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        if (service != null) {
            try {
                lock.lock();
                Resource resource = service.getResource(new AnyResourceIdImpl(arg0));
                if (resource != null) {
                    ResourceNode rn = new ResourceNode(Children.LEAF, resource, service.getName());
                    return rn;
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resource" + arg0, ex);
            } finally {
                lock.unlock();
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        if (service != null) {
            try {
                lock.lock();
                Resource resource = service.getResource(new AnyResourceIdImpl(arg0));
                if (resource != null) {
                    ResourceNode rn = new ResourceNode(Children.LEAF, resource, service.getName());
                    return new Node[]{rn};
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resource" + arg0, ex);
            } finally {
                lock.unlock();
            }
        }
        return null;
    }

    private class SCFServiceEventListener implements ServiceEventListener {

        @Override
        public boolean equals(Object o) {
            if (o instanceof SCFServiceEventListener) {
                if (getName() != null) {
                    return getName().equals(((SCFServiceEventListener) o).getName());
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 73 * hash + (getName() != null ? getName().hashCode() : 0);
            return hash;
        }

        public String getName() {
            return "SCFServiceEventListener" + counter;
        }

        public void resourceRequest(final ResourceRequestEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void addResource(final AddResourceEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceAdded(final ResourceAddedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void removeResource(final RemoveResourceEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceRemoved(final ResourceRemovedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceError(final ResourceErrorEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceReset(final ResourceResetEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void resourceChanged(final ResourceChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceStarting(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceRunning(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceUnknown(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceShutdown(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceError(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }

        public void serviceStopped(final ServiceStateChangedEvent event) {
            ServiceChildFactory.this.refresh(false);
        }
    }

    private class SCFComponentEventListener implements ComponentEventListener {

        public void componentUnknown(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void componentStarting(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void componentStarted(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void componentStopping(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void componentStopped(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void componentReloading(ComponentStateChangedEvent arg0) {
//            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}

