/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ResourceProviderChildFactory extends ChildFactory<String> {

    private ResourceProvider rp;
    private ResourceProviderEventListener rpel;
    private static long counter;
    private final Lock lock = new ReentrantLock();

    public ResourceProviderChildFactory(ResourceProvider rp) {
        if (rp == null) {
            throw new IllegalArgumentException("ResourceProvider must not be null");
        }
        this.rp = rp;
        this.rpel = new RPWResourceProviderEventListener();
        try {
            this.rp.addResourceProviderEventListener(rpel);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "error registering listener", e);
        }
        counter++;
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        arg0.clear();
        if (rp != null) {
            lock.lock();
            try {
                List<Resource> resources = rp.getResourcesInProcess();
                for (Resource r : resources) {
                    arg0.add(r.getId().getId());
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resources", ex);
            } finally {
                lock.unlock();
            }
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        if (rp != null) {
            lock.lock();
            try {
                List<Resource> resources = rp.getResourcesInProcess();
                Resource rslt = null;
                for (Resource r : resources) {
                    if (r.getId().getId().equals(arg0)) {
                        rslt = r;
                        break;
                    }
                }
                if (rslt != null) {
                    ResourceNode rn = new ResourceNode(Children.LEAF, rslt);
                    return rn;
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resources", ex);
            } finally {
                lock.unlock();
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        if (rp != null) {
            lock.lock();
            try {
                List<Resource> resources = rp.getResourcesInProcess();
                Resource rslt = null;
                for (Resource r : resources) {
                    if (r.getId().getId().equals(arg0)) {
                        rslt = r;
                        break;
                    }
                }
                if (rslt != null) {
                    ResourceNode rn = new ResourceNode(Children.LEAF, rslt);


                    return new Node[]{rn};
                }
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "error getting resources", ex);
            } finally {
                lock.unlock();
            }
        }
        return null;
    }

    private class RPWResourceProviderEventListener implements ResourceProviderEventListener {

        @Override
        public boolean equals(Object o) {
            if (o instanceof RPWResourceProviderEventListener) {
                if (getName() != null) {
                    return getName().equals(((RPWResourceProviderEventListener) o).getName());
                }
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 73 * hash + (getName() != null ? getName().hashCode() : 0);
            return hash;
        }

        public String getName() {
            return "RPWResourceProviderEventListener" + counter;
        }

        public void addResource(final AddResourceEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceAdded(final ResourceAddedEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void removeResource(final RemoveResourceEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceRemoved(final ResourceRemovedEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceError(final ResourceErrorEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceReset(final ResourceResetEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceChanged(final ResourceChangedEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }

        public void resourceRejected(final ResourceRejectedEvent event) {
            ResourceProviderChildFactory.this.refresh(true);
        }
    }
}
