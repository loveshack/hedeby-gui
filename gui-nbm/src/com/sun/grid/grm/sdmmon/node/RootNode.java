/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.sdmmon.options.OptionPanel;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.sdmmon.*;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.visual.graph.GraphScene;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class RootNode extends AbstractNode {

    private String name;
    private static final Map<ExecutionEnv, RootNode> instances = new HashMap<ExecutionEnv, RootNode>();
    private final static Lock lock = new ReentrantLock();
    private final ExecutionEnv env;

    RootNode(Children c, ExecutionEnv env) {
        super(c);
        this.env = env;
    }

    RootNode(Children c, Lookup l, ExecutionEnv env) {
        super(c, l);
        this.env = env;
    }

    public static final RootNode getInstance(ExecutionEnv env) {
        lock.lock();
        try {
            RootNode instance = instances.get(env);
            if (instance == null) {
                instance = new RootNode(Children.create(RootChildFactory.getInstance(), false), env);
                instances.put(env, instance);
            }
            return instance;
        } finally {
            lock.unlock();
        }
    }
    
    public static final RootNode getInstance(ExecutionEnv env, GraphScene<Node, Node> scene) {
        lock.lock();
        try {
            RootNode instance = instances.get(env);
            if (instance == null) {
                instance = new RootNode(Children.create(RootChildFactory.getInstance(scene), false), env);
                instances.put(env, instance);
            }
            return instance;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String getName() {
        if (name == null) {
            name = NbPreferences.forModule(OptionPanel.class).get(OptionPanel.SYSTEM, "");
        }
        return name;
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public String getHtmlDisplayName() {
        StringBuilder sb = new StringBuilder();
        sb.append("<font color='000000' size='2'>");
        sb.append(getDisplayName());
        sb.append("</font>");
        return sb.toString();
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        try {
            @SuppressWarnings("unchecked")
            Property idProp = new PropertySupport.Reflection(this, String.class,
                    "getDisplayName", null);

            idProp.setName("name");
            set.put(idProp);
            Property prop = new ReadOnlyProperty("CS host", env.getCSHost().getHostname(), "CS host", "CS host");
            set.put(prop);
            prop = new ReadOnlyProperty("CS port", String.valueOf(env.getCSPort()), "CS port", "CS port");
            set.put(prop);
            prop = new ReadOnlyProperty("CS url", env.getCSURL().toString(), "CS url", "CS url");
            set.put(prop);
            prop = new ReadOnlyProperty("Distribution", env.getDistDir().toString(), "Distribution", "Distribution");
            set.put(prop);
            prop = new ReadOnlyProperty("Local spool", env.getLocalSpoolDir().toString(), "Local spool", "Local spool");
            set.put(prop);
            for ( Map.Entry<String, ?> entry : env.getProperties().entrySet()) {
                prop = new ReadOnlyProperty(entry.getKey(), entry.getValue().toString(), entry.getKey(), entry.getKey());
                set.put(prop);
            }
        } catch ( NoSuchMethodException ex) {
                Logger.getLogger(getClass().getName()).log(Level.WARNING, "error in resource property sheet creation, no such method", ex);
        }
        sheet.put(set);
        return sheet;
    }

    private class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private final String value;

        private ReadOnlyProperty(String name, String value, String disp, String desc) {
            super(name, String.class, disp, desc);
            this.value = value;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return value;
        }
    }  
}
