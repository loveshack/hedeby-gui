/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.node;

import com.sun.grid.grm.sdmmon.scene.SdmSceneTopComponent;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.netbeans.api.visual.graph.GraphScene;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

public class RootChildFactory extends ChildFactory<String> {

    private GraphScene<Node, Node> nodesHolder;
    private static RootChildFactory instance;
    private final static Lock lock = new ReentrantLock();

    private RootChildFactory(GraphScene<Node, Node> scene) {
        this.nodesHolder = scene;
    }
    
    public void refresh() {
        this.refresh(false);
    }

    public static final RootChildFactory getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new RootChildFactory(SdmSceneTopComponent.findInstance().getSdmScene());
            }
            return instance;
        } finally {
            lock.unlock();
        }
    }
    
    public static final RootChildFactory getInstance(GraphScene<Node, Node> scene) {
        lock.lock();
        try {
            if (instance == null) {
                instance = new RootChildFactory(scene);
            }
            return instance;
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected boolean createKeys(List<String> arg0) {
        if (nodesHolder != null) {
            for (Node n : nodesHolder.getNodes()) {
                if (!(n instanceof RootNode) &&
                        !(n instanceof ResourceNode)) {
                    arg0.add(n.getDisplayName());
                }
            }
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(String arg0) {
        if (nodesHolder != null) {
            for (Node n : nodesHolder.getNodes()) {
                if (n.getDisplayName().equals(arg0) &&
                        !(n instanceof RootNode) &&
                        !(n instanceof ResourceNode)) {
                    return n;
                }
            }
        }
        return null;
    }

    @Override
    protected Node[] createNodesForKey(String arg0) {
        List<Node> nodez = new LinkedList<Node>();
        if (nodesHolder != null) {
            for (Node n : nodesHolder.getNodes()) {
                if (n.getDisplayName().equals(arg0) &&
                        !(n instanceof RootNode) &&
                        !(n instanceof ResourceNode)) {
                    nodez.add(n);
                }
            }
        }
        return nodez.toArray(new Node[nodez.size()]);
    }
}
