/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.util;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.sdmmon.options.OptionPanel;
import com.sun.grid.grm.security.CachingClientSecurityContextFactory;
import com.sun.grid.grm.security.SecurityContext;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.openide.util.NbPreferences;

public class ExecutionEnvWrapper implements ExecutionEnv {

    private static ExecutionEnvWrapper singleton;
    private static ExecutionEnv instance;
    private static Map<String, String> prefs = new HashMap<String, String>();
    private static CallbackHandler callbackHandler;
    private static Set<ExecutionEnvWrapperListener> listeners = new HashSet<ExecutionEnvWrapperListener>();
    private static CachingClientSecurityContextFactory ccscf = new CachingClientSecurityContextFactory();
    

    static {
        NbPreferences.forModule(OptionPanel.class).addPreferenceChangeListener(new PreferenceChangeListener() {

            public void preferenceChange(final PreferenceChangeEvent evt) {
                String oldValue = prefs.get(evt.getKey());
                if (oldValue == null || !oldValue.equals(evt.getNewValue())) {
                    if (evt.getKey().equals(OptionPanel.USER)) {
                        callbackHandler = new UICallbackHandler();
                    }
                    try {
                        instance = getExecEnv();
                        for (ExecutionEnvWrapperListener l : listeners) {
                            l.instanceHasChanged(instance);
                        }
                    } catch (GrmException grex) {
                        Logger.getLogger(getClass().getName()).log(Level.INFO, "failed to get execution environment", grex);
                    }
                }
            }
        });
    }

    private static void readPrefs() {
        Preferences pref = NbPreferences.forModule(OptionPanel.class);
        prefs.put(OptionPanel.SYSTEM, pref.get(OptionPanel.SYSTEM, ""));
        prefs.put(OptionPanel.PREF_TYPE_STR, pref.get(OptionPanel.PREF_TYPE_STR, "USER"));
        prefs.put(OptionPanel.USER, pref.get(OptionPanel.USER, ""));
        prefs.put(OptionPanel.PASS, pref.get(OptionPanel.PASS, ""));
    }

    private ExecutionEnvWrapper() throws GrmException {
        if (callbackHandler == null) {
            callbackHandler = new UICallbackHandler();
        }
        if (instance == null) {
            instance = getExecEnv();
        }
    }

    public static void addExecutionEnvWrapperListener(ExecutionEnvWrapperListener listener) {
        listeners.add(listener);
    }

    public static void removeExecutionEnvWrapperListener(ExecutionEnvWrapperListener listener) {
        listeners.remove(listener);
    }

    public static ExecutionEnv getInstance() throws GrmException {
        if (singleton == null) {
            singleton = new ExecutionEnvWrapper();
        }
        return singleton;
    }

    private static ExecutionEnv getExecEnv() throws GrmException {
        // Use system prefsType as default        
        readPrefs();
        PreferencesType pt = PreferencesType.valueOf(prefs.get(OptionPanel.PREF_TYPE_STR));
        ExecutionEnv ret = null;
        try {
            String csUrl = PreferencesUtil.getCSInfo(prefs.get(OptionPanel.SYSTEM), pt);
            HostAndPort hp = HostAndPort.newInstance(csUrl);
            ccscf.registerContext(hp, prefs.get(OptionPanel.USER), callbackHandler);
            ret = ExecutionEnvFactory.newClientInstance(prefs.get(OptionPanel.SYSTEM), pt, ccscf);
        } catch (BackingStoreException ex) {
            throw new GrmException("ExecutionEnv can not be created", ex);
        }
        return ret;
    }

    public String getSystemName() {
        return instance.getSystemName();
    }

    public Context getContext() throws GrmException {
        return instance.getContext();
    }

    public CommandService getCommandService() {
        return instance.getCommandService();
    }

    public JMXServiceURL getCSURL() {
        return instance.getCSURL();
    }

    public SecurityContext getSecurityContext() {
        return instance.getSecurityContext();
    }

    public Hostname getCSHost() {
        return instance.getCSHost();
    }

    public int getCSPort() {
        return instance.getCSPort();
    }

    public File getDistDir() {
        return instance.getDistDir();
    }

    public File getLocalSpoolDir() {
        return instance.getLocalSpoolDir();
    }

    public Map<String, ?> getProperties() {
        return instance.getProperties();
    }

    public static class UICallbackHandler implements CallbackHandler {

        protected void handle(Callback cb) throws IOException,
                UnsupportedCallbackException {
            readPrefs();
            String user = prefs.get(OptionPanel.USER);
            String pass = prefs.get(OptionPanel.PASS);
            if (cb instanceof PasswordCallback) {
                PasswordCallback pwCallback = (PasswordCallback) cb;
                pwCallback.setPassword(pass.toCharArray());
            } else if (cb instanceof NameCallback) {
                NameCallback ncb = (NameCallback) cb;
                ncb.setName(user);
            } else {
                throw new UnsupportedCallbackException(cb);
            }
        }

        /**
         * Handle callbacks.
         *
         * @param callbacks  the callback
         * @throws java.io.IOException on any IO error
         * @throws javax.security.auth.callback.UnsupportedCallbackException if
         *                the unsupported callback is found
         */
        public final void handle(final Callback[] callbacks) throws IOException,
                UnsupportedCallbackException {
            for (Callback cb : callbacks) {
                handle(cb);
            }
        }
    }

    public ResourceProvider getResourceProvider() {
        ResourceProvider ret = null;
        try {
            ret = ComponentService.<ResourceProvider>getComponentByType(ExecutionEnvWrapper.getInstance(), ResourceProvider.class);
        } catch (GrmException e) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "failed to retrieve RP proxy", e);
        }
        return ret;
    }

    public HostAndPort getCSInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
