/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.scene;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.sdmmon.node.RootNode;
import com.sun.grid.grm.sdmmon.options.OptionPanel;
import com.sun.grid.grm.sdmmon.scene.SdmSceneImpl;
import com.sun.grid.grm.sdmmon.scene.SdmScene;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapperListener;
import java.awt.BorderLayout;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.api.visual.model.ObjectSceneEvent;
import org.netbeans.api.visual.model.ObjectSceneEventType;
import org.netbeans.api.visual.model.ObjectSceneListener;
import org.netbeans.api.visual.model.ObjectState;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * Top component which displays something.
 */
public final class SdmSceneTopComponent extends TopComponent implements ExplorerManager.Provider {

    private static final Logger log = Logger.getLogger(SdmSceneTopComponent.class.getName());
    private final static long serialVersionUID = -2007010301L;
    private static SdmSceneTopComponent instance;
    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "com/sun/grid/grm/sdmmon/139.png";
    private static final String PREFERRED_ID = "SdmSceneTopComponent";
    private JComponent mainView;
    private SdmScene scene;
    private final ExplorerManager mgr = new ExplorerManager();

    private SdmSceneTopComponent() {

        initComponents();
        setName(NbBundle.getMessage(SdmSceneTopComponent.class, "CTL_SdmSceneTopComponent"));
        setToolTipText(NbBundle.getMessage(SdmSceneTopComponent.class, "HINT_SdmSceneTopComponent"));
        setIcon(Utilities.loadImage(ICON_PATH, true));
        scene = new SdmSceneImpl();
        scene.addObjectSceneListener(new MyObjectSceneListener(), ObjectSceneEventType.OBJECT_SELECTION_CHANGED);
        mainView = scene.createView();

        mainPane.setViewportView(mainView);
        satellitePanel.add(scene.createSatelliteView(), BorderLayout.CENTER);
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));

        Preferences pref = NbPreferences.forModule(OptionPanel.class);
        pref.addPreferenceChangeListener(new PreferenceChangeListener() {

            public void preferenceChange(final PreferenceChangeEvent evt) {
                if (evt.getKey().equals(OptionPanel.SYSTEM)) {
                    SwingUtilities.invokeLater(new Runnable() {

                        public void run() {
                            SdmSceneTopComponent.this.setToolTipText(evt.getNewValue());
                        }
                    });
                }
            }
        });
        ExecutionEnvWrapper.addExecutionEnvWrapperListener(new MyExecutionEnvWrapperListener());
        try {
            mgr.setRootContext(RootNode.getInstance(ExecutionEnvWrapper.getInstance(), scene));
        } catch (GrmException grex) {
            log.log(Level.WARNING, "root context can not be set, execution enviroment can not be created", grex);
        }
    }

    public SdmScene getSdmScene() {
        return scene;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        satellitePanel = new javax.swing.JPanel();
        mainPane = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        jSplitPane1.setDividerLocation(jSplitPane1.getWidth());

        satellitePanel.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setRightComponent(satellitePanel);

        mainPane.setPreferredSize(new java.awt.Dimension(640, 480));
        jSplitPane1.setLeftComponent(mainPane);

        add(jSplitPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JScrollPane mainPane;
    private javax.swing.JPanel satellitePanel;
    // End of variables declaration//GEN-END:variables

    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link findInstance}.
     * @return 
     */
    public static synchronized SdmSceneTopComponent getDefault() {
        if (instance == null) {
            instance = new SdmSceneTopComponent();
        }

        return instance;
    }

    /**
     * Obtain the SdmSceneTopComponent instance. Never call {@link #getDefault} directly!
     * @return 
     */
    public static synchronized SdmSceneTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(SdmSceneTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");


            return getDefault();
        }
        if (win instanceof SdmSceneTopComponent) {
            return (SdmSceneTopComponent) win;
        }



        Logger.getLogger(SdmSceneTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID +
                "' ID. That is a potential source of errors and unexpected behavior.");


        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_NEVER;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    /** replaces this in object stream
     * @return 
     */
    @Override
    public Object writeReplace() {
        return new ResolvableHelper();
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    final static class ResolvableHelper implements Serializable {

        private static final long serialVersionUID = 1L;

        public Object readResolve() {
            return SdmSceneTopComponent.getDefault();
        }
    }

    public ExplorerManager getExplorerManager() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private class MyObjectSceneListener implements ObjectSceneListener {

        public void objectAdded(ObjectSceneEvent arg0, Object arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void objectRemoved(ObjectSceneEvent arg0, Object arg1) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void objectStateChanged(ObjectSceneEvent arg0, Object arg1, ObjectState arg2, ObjectState arg3) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void selectionChanged(ObjectSceneEvent arg0, Set<Object> arg1, Set<Object> arg2) {
            if (!arg2.isEmpty()) {
                Node n = (Node) arg2.iterator().next();
                try {
                    mgr.setRootContext(n);
                    mgr.setSelectedNodes(new Node[]{n});
                } catch (PropertyVetoException ex) {
                    log.log(Level.INFO, "node:" + n + " can not be selected");
                }
            } else {
                try {
                    mgr.setSelectedNodes(new Node[]{});
                } catch (PropertyVetoException ex) {
                    log.log(Level.INFO, "selection can not be cleared");
                }
            }
        }

        public void highlightingChanged(ObjectSceneEvent arg0, Set<Object> arg1, Set<Object> arg2) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void hoverChanged(ObjectSceneEvent arg0, Object arg1, Object arg2) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void focusChanged(ObjectSceneEvent arg0, Object arg1, Object arg2) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class MyExecutionEnvWrapperListener implements ExecutionEnvWrapperListener {

        public void instanceHasChanged(ExecutionEnv instance) {
            mgr.setRootContext(RootNode.getInstance(instance));
        }
    }
}
