/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sdmmon.scene;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.sdmmon.edge.RPResourceEdge;
import com.sun.grid.grm.sdmmon.edge.RootEdge;
import com.sun.grid.grm.sdmmon.edge.ServiceResourceEdge;
import com.sun.grid.grm.sdmmon.node.ResourceNode;
import com.sun.grid.grm.sdmmon.widget.ServiceWidget;
import com.sun.grid.grm.sdmmon.node.ResourceProviderNode;
import com.sun.grid.grm.sdmmon.node.RootNode;
import com.sun.grid.grm.sdmmon.node.ServiceNode;
import com.sun.grid.grm.sdmmon.scene.layout.SdmSceneLayout;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapper;
import com.sun.grid.grm.sdmmon.util.ExecutionEnvWrapperListener;
import com.sun.grid.grm.sdmmon.widget.RPResourceEdgeWidget;
import com.sun.grid.grm.sdmmon.widget.ResourceProviderWidget;
import com.sun.grid.grm.sdmmon.widget.ResourceWidget;
import com.sun.grid.grm.sdmmon.widget.RootEdgeWidget;
import com.sun.grid.grm.sdmmon.widget.RootWidget;
import com.sun.grid.grm.sdmmon.widget.ServiceResourceEdgeWidget;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.ConnectProvider;
import org.netbeans.api.visual.action.ConnectorState;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.graph.layout.GraphLayout;
import org.netbeans.api.visual.graph.layout.GraphLayoutFactory;
import org.netbeans.api.visual.graph.layout.GraphLayoutSupport;
import org.netbeans.api.visual.graph.layout.GridGraphLayout;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class SdmSceneImpl extends SdmScene implements ExecutionEnvWrapperListener {

    private LayerWidget cachedLayer;
    private LayerWidget connectionLayer;
    private LayerWidget interactionLayer;
    private WidgetAction moveAction;
    private SceneLayout sceneGraphLayout;
    private SceneLayout sceneGridLayout;
    private SceneLayout sceneSDMLayout;
    private GraphLayout<Node, Node> graphLayout;
    private GraphLayout<Node, Node> sdmLayout;
    private final Lock lock = new ReentrantLock();
    private SdmSceneSupportedLayoutType myLayout = SdmSceneSupportedLayoutType.SDM_LAYOUT;

    public SdmSceneImpl() {
        cachedLayer = new LayerWidget(this);
        connectionLayer = new LayerWidget(this);
        interactionLayer = new LayerWidget(this);

        addChild(cachedLayer);
        addChild(connectionLayer);
        addChild(interactionLayer);

        moveAction = ActionFactory.createAlignWithMoveAction(cachedLayer, interactionLayer, null);

        getActions().addAction(ActionFactory.createZoomAction());
        getActions().addAction(ActionFactory.createPanAction());

    }

    /**
     * Allows to set preferred layout. 
     * 
     * @param layout preferred layout
     * @return instance of scene
     */
    public SdmSceneImpl setLayout(SdmSceneSupportedLayoutType layout) {
        myLayout = layout;
        return this;
    }

    private void init() {
        lock.lock();
        try {
            sdmLayout = new SdmSceneLayout(100, 100);
            graphLayout = GraphLayoutFactory.createTreeGraphLayout(100, 100, 50, 50, false);
            GraphLayout<Node, Node> gridLayout = new GridGraphLayout<Node, Node>().setChecker(true);
            try {
                GraphLayoutSupport.setTreeGraphLayoutRootNode(graphLayout, RootNode.getInstance(ExecutionEnvWrapper.getInstance()));
            } catch (GrmException grex) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "RootNode is not available");
            }
            sceneGraphLayout = LayoutFactory.createSceneGraphLayout(this, graphLayout);
            sceneGridLayout = LayoutFactory.createSceneGraphLayout(this, gridLayout);
            sceneSDMLayout = LayoutFactory.createSceneGraphLayout(this, sdmLayout);
        } finally {
            lock.unlock();
        }
    }

    private void performSdmLayout() {
        if (sceneSDMLayout == null) {
            init();
        }
        sceneSDMLayout.invokeLayoutImmediately();
        this.validate();
    }

    private void performTreeLayout() {
        if (sceneGraphLayout == null) {
            init();
        }
        sceneGraphLayout.invokeLayoutImmediately();
    }

    private void performGridLayout() {
        if (sceneGridLayout == null) {
            init();
        }
        sceneGridLayout.invokeLayoutImmediately();
    }

    public void performLayout() {
        switch (myLayout) {
            case SDM_LAYOUT:
                performSdmLayout();
                break;
            case GRID_LAYOUT:
                performGridLayout();
                break;
            case TREE_LAYOUT:
                performTreeLayout();
                break;
            default:
                break;
        }
    }

    public void setCachedLayerVisible(boolean visible) {
        findWidget(cachedLayer).setVisible(visible);
    }

    public void setNonCachedLayerVisible(boolean visible) {
        findWidget(cachedLayer).setVisible(visible);
    }

    @Override
    protected Widget attachNodeWidget(Node node) {
        Widget widget = null;
        if (node instanceof ServiceNode) {
            widget = new ServiceWidget(this, (ServiceNode) node);
        } else if (node instanceof ResourceProviderNode) {
            widget = new ResourceProviderWidget(this, (ResourceProviderNode) node);
        } else if (node instanceof ResourceNode) {
            widget = new ResourceWidget(this, (ResourceNode) node);
        } else if (node instanceof RootNode) {
            widget = new RootWidget(this, (RootNode) node);
        }
        if (widget != null) {
            //single-click, the event is not consumed:
            widget.getActions().addAction(createSelectAction());
            //mouse-dragged, the event is consumed while mouse is dragged:
            widget.getActions().addAction(moveAction);
            //mouse-over, the event is consumed while the mouse is over the widget:
            widget.getActions().addAction(createWidgetHoverAction());
            cachedLayer.addChild(widget);
        }
        validate();
        return widget;
    }

    @Override
    protected void detachNodeWidget (Node node, Widget widget) {        
        if (widget != null)
            widget.removeFromParent ();
    }
    
    @Override
    protected Widget attachEdgeWidget(Node edge) {
        if (edge instanceof ServiceResourceEdge) {
            ServiceResourceEdgeWidget connection = new ServiceResourceEdgeWidget(this, (ServiceResourceEdge) edge);
            connectionLayer.addChild(connection);
            validate();
            return connection;
        } else if (edge instanceof RPResourceEdge) {
            RPResourceEdgeWidget connection = new RPResourceEdgeWidget(this, (RPResourceEdge) edge);
            connectionLayer.addChild(connection);
            validate();
            return connection;
        } else {
            RootEdgeWidget connection = new RootEdgeWidget(this, (RootEdge) edge);
            connectionLayer.addChild(connection);
            validate();
            return connection;
        }
    }

    @Override
    protected void attachEdgeSourceAnchor(Node edge, Node oldSourceNode, Node sourceNode) {
        Widget w = sourceNode != null ? findWidget(sourceNode) : null;
        ((ConnectionWidget) findWidget(edge)).setSourceAnchor(AnchorFactory.createRectangularAnchor(w));
        validate();
    }

    @Override
    protected void attachEdgeTargetAnchor(Node edge, Node oldTargetNode, Node targetNode) {
        Widget w = targetNode != null ? findWidget(targetNode) : null;
        ((ConnectionWidget) findWidget(edge)).setTargetAnchor(AnchorFactory.createRectangularAnchor(w));
        validate();
    }

    private class SceneConnectProvider implements ConnectProvider {

        private Node source = null;
        private Node target = null;

        public boolean isSourceWidget(Widget sourceWidget) {
            Object object = findObject(sourceWidget);
            source = isNode(object) ? (Node) object : null;
            return source != null;
        }

        public boolean hasCustomTargetWidgetResolver(Scene scene) {
            return false;
        }

        public Widget resolveTargetWidget(Scene scene, Point sceneLocation) {
            return null;
        }

        public void createConnection(Widget sourceWidget, Widget targetWidget) {
            if ((sourceWidget instanceof ServiceWidget) && (targetWidget instanceof ResourceWidget)) {
                ServiceResourceEdge sre = new ServiceResourceEdge(Children.LEAF, (ServiceWidget) sourceWidget, (ResourceWidget) targetWidget);
                addEdge(sre);
                setEdgeSource(sre, source);
                setEdgeTarget(sre, target);
            }
        }

        public ConnectorState isTargetWidget(Widget sourceWidget, Widget targetWidget) {
            Object object = findObject(targetWidget);
            target = isNode(object) ? (Node) object : null;
            if (target != null) {
                return !source.equals(target) ? ConnectorState.ACCEPT : ConnectorState.REJECT_AND_STOP;
            }
            return object != null ? ConnectorState.REJECT_AND_STOP : ConnectorState.REJECT;
        }
    }

    public void instanceHasChanged(ExecutionEnv instance) {
        GraphLayoutSupport.setTreeGraphLayoutRootNode(graphLayout, RootNode.getInstance(instance));
    }

    @Override
    public RootNode getRootNode() throws GrmException {
        RootNode rootNode = RootNode.getInstance(ExecutionEnvWrapper.getInstance());
        if (rootNode != null && getNodes().contains(rootNode)) {
            return rootNode;
        } else {
            throw new GrmException("no root node yet");
        }
    }

    @Override
    public ResourceProviderNode getResourceProviderNode() throws GrmException {
        Node rootNode = getRootNode();
        if (rootNode != null && getNodes().contains(rootNode)) {
            /* we need to look just for 'output edges' */
            Collection<Node> rootEdges = findNodeEdges(rootNode, true, false);
            /* rootNode has connection only with ServiceNodes and RPNode */
            for (Node n : rootEdges) {
                if (getEdgeTarget(n) instanceof ResourceProviderNode) {
                    return (ResourceProviderNode) getEdgeTarget(n);
                }
            }
            throw new GrmException("no RP node yet");
        } else {
            throw new GrmException("no RP node yet");
        }
    }

    @Override
    public Collection<ServiceNode> getServiceNodes() {
        Node rootNode = null;
        try {
            rootNode = getRootNode();
        } catch (GrmException ex) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "root node is not yet on the scene", ex);
        }
        if (rootNode != null && getNodes().contains(rootNode)) {
            Collection<ServiceNode> serviceNodes = new LinkedList<ServiceNode>();
            /* we need to look just for 'output edges' */
            Collection<Node> rootEdges = findNodeEdges(rootNode, true, false);
            /* rootNode has connection only with ServiceNodes and RPNode */
            for (Node n : rootEdges) {
                if (getEdgeTarget(n) instanceof ServiceNode) {
                    serviceNodes.add((ServiceNode) getEdgeTarget(n));
                }
            }
            return serviceNodes;
        } else {
            return Collections.<ServiceNode>emptyList();
        }
    }

    @Override
    public Collection<ResourceNode> getResourceNodes(ServiceNode serviceNode) {
        Collection<ServiceNode> serviceNodez = getServiceNodes();
        Collection<ResourceNode> resourceNodez = new LinkedList<ResourceNode>();
        for (ServiceNode node : serviceNodez) {
            if (serviceNode.equals(node)) {
                /* we need to look just for 'output edges' */
                Collection<Node> serviceEdges = findNodeEdges(node, true, false);
                /* serviceNode has output connection only with its ResourceNodes */
                for (Node n : serviceEdges) {
                    if (getEdgeTarget(n) instanceof ResourceNode) {
                        resourceNodez.add((ResourceNode) getEdgeTarget(n));
                    }
                }
            }
        }
        return resourceNodez;
    }

    @Override
    public Collection<ResourceNode> getResourceNodes(ResourceProviderNode rpNode) {
        Collection<ResourceNode> resourceNodez = new LinkedList<ResourceNode>();
        /* we need to look just for 'output edges' */
        Collection<Node> rpEdges = findNodeEdges(rpNode, true, false);
        /* serviceNode has output connection only with its ResourceNodes */
        for (Node n : rpEdges) {
            if (getEdgeTarget(n) instanceof ResourceNode) {
                resourceNodez.add((ResourceNode) getEdgeTarget(n));
            }
        }
        return resourceNodez;
    }

    @Override
    public void clearScene() {
        List<Node> nodes = new ArrayList<Node>(getNodes());
        for (Node n : nodes) {
            removeNode(n);
        }
        validate();
    }
}
