/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.sdmmon.scene;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.sdmmon.node.ResourceNode;
import com.sun.grid.grm.sdmmon.node.ResourceProviderNode;
import com.sun.grid.grm.sdmmon.node.RootNode;
import com.sun.grid.grm.sdmmon.node.ServiceNode;
import java.util.Collection;
import org.netbeans.api.visual.graph.GraphScene;
import org.openide.nodes.Node;

public abstract class SdmScene extends GraphScene<Node, Node> {

    public abstract void performLayout();   

    public abstract void setCachedLayerVisible(boolean visible);

    public abstract void setNonCachedLayerVisible(boolean visible);
           
    public abstract RootNode getRootNode() throws GrmException;
    
    public abstract ResourceProviderNode getResourceProviderNode() throws GrmException;
    
    public abstract Collection<ServiceNode> getServiceNodes();
    
    public abstract Collection<ResourceNode> getResourceNodes(ServiceNode serviceNode);
    
    public abstract Collection<ResourceNode> getResourceNodes(ResourceProviderNode rpNode);
    
    public abstract void clearScene();
}
